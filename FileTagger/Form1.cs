﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace FileTagger
{
    public partial class Form1 : Form
    {
        private string tagFilePath = "E:\\Dropbox\\Tagi.txt";
        FileExplorer fileExplorer = new FileExplorer("E:");
        FileTagger fileTagger= new FileTagger();
        public Form1()
        {
            InitializeComponent();
            var l = fileExplorer.GetList();
            FillListBox(l);
            listBox1.SelectionMode=SelectionMode.MultiExtended;
            fileTagger.LoadTags(tagFilePath);
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                string name = listBox1.SelectedItem.ToString();
                try
                {
                    var l = fileExplorer.Execute(name);
                    FillListBox(l);
                }
                catch (FileNotFoundException e2)
                {
                    MessageBox.Show(e2.Message);
                }
            }
        }

        private void filterTextBox_TextChanged(object sender, EventArgs e)
        {
            if (filterTextBox.Text != "")
            {
                listBox1.Items.Clear();
                var l = fileTagger.GetFileList(filterTextBox.Text);
                foreach (var file in l)
                {
                    listBox1.Items.Add(file);
                }
            }
            else
            {
                FillListBox(fileExplorer.GetList());
            }
        }

        private void addTagButton_Click(object sender, EventArgs e)
        {
            foreach (var file in listBox1.SelectedItems)
            {
                fileTagger.SetTags(fileExplorer.GetActualPath()+'\\'+FileExplorer.SubstractDirName(file.ToString())+'\\',tagTextBox.Text);
            }
            
        }

        private void listBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (listBox1.SelectedItem != null)
                    FillListBox(fileExplorer.Execute(listBox1.SelectedItem?.ToString()));
            }
            else if (e.KeyCode == Keys.Back)
                FillListBox(fileExplorer.Execute("[..]"));
        }

        private void FillListBox(List<string> elements )
        {
            listBox1.Items.Clear();
            foreach (var element in elements)
            {
                listBox1.Items.Add(element);
            }
            if(listBox1.Items.Count>0)
                listBox1.SetSelected(0,true);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            fileTagger.saveTags(tagFilePath);
        }

        private void listBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Enter && e.KeyChar != (char) Keys.Back)
            {
                filterTextBox.Text += e.KeyChar;
                filterTextBox.Focus();
            }

        }
    }
}
