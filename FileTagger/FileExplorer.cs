﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileTagger
{
    class FileExplorer
    {
        public static String SubstractDirName(string name)
        {
            if (name.StartsWith("["))
            {
                name = name.Substring(1, name.Length - 2);
            }
            return name;
        }

        public static String SubstractFileFromPath(string path)
        {
            return path.Split('\\').Last();
        }

        private string actualPath = "";

        public FileExplorer(string actualPath)
        {
            this.actualPath = actualPath;
        }

        public bool CanGoUpper()
        {
            return actualPath.Split('\\').Length>1;
        }

        public List<string> ChangeDir(string newPath)
        {
            List<string>directoryList = new List<string>();
            if (newPath == "[..]")
            {
                GoUpper();
            }
            else
            {
                newPath = SubstractDirName(newPath);

                if (Directory.Exists(actualPath + '\\' + newPath))
                    actualPath = actualPath + '\\' + newPath;
            }
            return GetList();
        }

        public void GoUpper()
        {
            string newPath;
            if(CanGoUpper())
                newPath = actualPath.Remove(actualPath.Length - actualPath.Split('\\').Last().Length - 1);
            else
                newPath = actualPath;
            if (Directory.Exists(newPath))
                actualPath = newPath;
        }

        public List<string> GetList()
        {
            List<string> directoryList = new List<string>();
            string path = actualPath;
            if (Directory.Exists(path))
            {
                actualPath = path;
                if(CanGoUpper())
                    directoryList.Add("[..]");
                var directories = Directory.GetDirectories(path+'\\');
                foreach (var dir in directories)
                {
                    directoryList.Add('[' + dir.Split('\\')[dir.Split('\\').Length - 1] + ']');
                }
                var fileList = Directory.GetFiles(path+'\\').ToList();
                foreach (var file in fileList)
                {
                    directoryList.Add(file.Split('\\')[file.Split('\\').Length - 1]);
                }
            }
            return directoryList;
        }

        public string GetActualPath()
        {
            return actualPath;
        }

        public List<string> Execute(string path)
        {
            if (path.StartsWith("[") || Directory.Exists(path))
            {
                return ChangeDir(path);
            }
            else
            {
                if (File.Exists(actualPath+'\\'+ path))
                    System.Diagnostics.Process.Start(actualPath + '\\' + path);
                else
                {
                    throw new FileNotFoundException("Couldn't find file \""+path+"\"");
                }
            }
            return GetList();
        }
    }
}
