﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileTagger
{
    class FileTagger
    {
        private List<string> tagList=new List<string>();

        public void SetTags(string path, string tags)
        {
            tagList.Add(path+"*"+tags);
        }

        public string GetTags(string path)
        {
            return tagList.First(e => e.Split('*')[0] == path).Split('*')[1];
        }

        public void saveTags(string path)
        {
            File.WriteAllLines(path,tagList);
        }

        public void LoadTags(string path)
        {
            if (File.Exists(path))
                tagList.AddRange(File.ReadAllLines(path).ToList());
        }

        public IEnumerable<string> GetFileList(string tags)
        {
            HashSet<string> result = new HashSet<string>();
            var internalTagList = tags.Split(' ');
            foreach (var tag in internalTagList)
            {
                result.UnionWith(tagList.FindAll(x=>x.Contains(tag)));
            }

            return result.ToList().ConvertAll(x=>x.Split('*')[0]);
        }

    }
}
